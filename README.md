<br/>
<p align="center">
  <h1 align="center">WellFB</h1>

## About The Project

<img src="./github_images/preview.png">
<br/>
I started this project with a clear objective in mind: to elevate my proficiency in Spring Boot and Java programming. Over the course of about four months, I dedicated myself to its development, delving into various intricacies and challenges along the way. With each line of code written and every problem solved, I gained invaluable insights and practical experience that contributed to my growth as a developer.

Despite reaching a stage of completion, the journey doesn't end here. This project is an ongoing endeavor, with continuous improvements and enhancements planned for the future. As I delve deeper into its development, I am constantly exploring new features, refining existing functionalities, and staying abreast of the latest advancements in the field.

In addition to honing my technical skills, this project has also taught me invaluable lessons in project management, problem-solving, and collaboration. It has provided me with a platform to apply theoretical knowledge in a real-world context, solidifying my understanding of software development principles and best practices.

As I continue to invest my time and effort into this project, I am excited about the possibilities that lie ahead. With each milestone achieved, I am one step closer to realizing my vision and achieving my goals as a software developer.

## Built With

This project is written in Java, using Spring-Boot for backend and Thymeleaf as Rendering Engine.

* [Java](https://www.java.com/)
* [Spring-Boot](https://spring.io/projects/spring-boot/)
* [Thymeleaf](https://www.thymeleaf.org/)
* [jQuerry](https://jquery.com/)
* [Charts.js](https://www.chartjs.org/)
* [Bootstrap](https://getbootstrap.com/)

## Features

### Comment your favourite posts

<img src="./github_images/comment.png">

### Follow your friends

<img src="./github_images/follow_friends.png">

### Share memories

<img src="./github_images/upload.png">


## Be in control as an Admin

<img src="./github_images/admin_preview.png">
<img src="./github_images/admin_posts.png">
<img src="./github_images/admin_users.png">


## Installation (for developers)

**In order to start the project an ACTIVE internet connection is required.**

### Windows

1. Please ensure that JDK 21 (Java Development Kit) is installed, if not, install it from [here](https://www.oracle.com/ch-de/java/technologies/downloads/#jdk21-windows).

2. [Add the JDK to path](https://www.geeksforgeeks.org/how-to-set-java-path-in-windows-and-linux/)

* Add a new System Variable `JAVA_HOME` with the value `C:\Program Files\Java\jdk21`

* Under System Variables > Path, insert `%JAVA_HOME%\bin`

3. Clone the repository on your local machine

```bash
git clone https://github.com/claudiu-sava/WellFB.git
```

4. Go to the Battleship folder

5. Start the project using

```bash
mvnw.cmd spring-boot:run
```

6. On your browser open `http://localhost:8080`

### Linux / Unix / Mac

1. Please ensure that JDK 21 is installed. *Use your system instructions to install it*

2. Add the JDK to path

```bash
# JDK 21 should be found here /usr/lib/jvm/<jdk 21>
# Replace <jdk 21> with the actual folder name

sudo nano /etc/profile


# Add at the end of the file

export JAVA_HOME="<jdk path>"
export PATH=$JAVA_HOME/bin:$PATH


# Save and exit nano
# Apply changes using

source /etc/profile
```

3. Clone the repository on your local machine

```bash
git clone https://github.com/claudiu-sava/WellFB.git
```

4. Go to the Battleship folder

5. Start the project using

```bash
mvn spring-boot:run
```

6. On your browser open `http://localhost:8080`


### IntelliJ IDEA

You can start the project in IntelliJ without having to manually configure Java. Please refer to IntelliJ's instructions on how to install the SDK 21 and how to install the dependencies using Maven.


## Roadmap

See the [open issues](https://github.com/claudiu-sava/WellFB/issues) for a list of proposed features (and known issues).

### Psst, bugs

WellFB is far from perfect. If you encounter any bugs or problems, don't hesitate to create an issue.

## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.
* If you have suggestions for adding or removing projects, feel free to [open an issue](https://github.com/claudiu-sava/WellFB/issues/new) to discuss it, or directly create a pull request after you edit the *README.md* file with necessary changes.
* Please make sure you check your spelling and grammar.
* Create individual PR for each suggestion.


### Creating A Pull Request

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

## Authors

* **Claudiu Sava** - *Student / Junior Java Backend dev* - [Claudiu Sava](https://github.com/claudiu-sava)
