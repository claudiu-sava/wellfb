package com.claudiusava.WellFB.controller;

import com.claudiusava.WellFB.service.DatabaseConfigurator;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.WebAttributes;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.naming.AuthenticationException;

@Controller
public class LoginController {

    @Autowired
    private DatabaseConfigurator databaseConfigurator;

    @GetMapping("/login")
    public String loginPage(Model model){

        model.addAttribute("title", "Login");

        databaseConfigurator.configureDatabase();

        return "login";

    }

    @GetMapping("/login-error")
    public String login(HttpServletRequest request,
                        Model model) {
        HttpSession session = request.getSession(false);
        String errorMessage = null;
        if (session != null) {
            AuthenticationException ex = (AuthenticationException) session
                    .getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
            if (ex != null) {
                errorMessage = ex.getMessage();
            }
        }
        model.addAttribute("errorMessage", errorMessage);
        return "login";
    }

}
