package com.claudiusava.WellFB.service;

import com.claudiusava.WellFB.model.Avatar;
import com.claudiusava.WellFB.model.Role;
import com.claudiusava.WellFB.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;

import static com.claudiusava.WellFB.security.SecurityConfiguration.passwordEncoder;

@Service
public class DatabaseConfigurator {

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private AvatarService avatarService;


    public void configureDatabase() {

        configureRoles();
        configureAdminUser();

    }

    private void configureAdminUser(){

        if (userService.getUserByUsername("admin") == null) {

            User admin = new User();
            admin.setUsername("admin");
            admin.setPassword(passwordEncoder().encode("admin"));

            Role roles = roleService.getRoleByName("ROLE_ADMIN");
            admin.setRoles(Collections.singleton(roles));

            admin.setPosts(null);
            admin.setFollows(null);
            admin.setFollowedBy(null);

            Avatar userAvatar = new Avatar();
            userAvatar.setFileName("/avatars/default_avatar_100.png");
            avatarService.saveAvatar(userAvatar);

            admin.setAvatar(userAvatar);

            userService.saveChangesToUser(admin);

        }

    }

    private void configureRoles() {

        if (roleService.getRoleByName("ROLE_USER") == null) {
            Role userRole = new Role();
            userRole.setName("ROLE_USER");

            roleService.saveRole(userRole);
        }

        if (roleService.getRoleByName("ROLE_ADMIN") == null) {
            Role adminRole = new Role();
            adminRole.setName("ROLE_ADMIN");

            roleService.saveRole(adminRole);
        }

    }

}
