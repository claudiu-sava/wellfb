package com.claudiusava.WellFB.dto;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class NewPostDto {

    private String commentBody;
    private int postId;

}
