package com.claudiusava.WellFB.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.annotation.web.configurers.LogoutConfigurer;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;


@Configuration
@EnableWebSecurity
public class SecurityConfiguration{

    private static final String[] admin_auth = {
            "/admin/**"
    };

    private static final String[] user_auth = {
            "/",
            "/post/**",
            "/uploads/**",
            "hq/**"
    };

    private static final String[] anon_auth = {
            "/css/styles.css",
            "/js/script.js",
            "/users/new",
            "/drawable/**",
            "/avatars/default_avatar_100.png",
            "/fragments/**",
            "/login"
    };

    @Bean
    SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http.authorizeHttpRequests(request -> request
                        .requestMatchers(admin_auth).hasRole("ADMIN")
                        .requestMatchers(user_auth).hasAnyRole("USER", "ADMIN")
                        .requestMatchers(anon_auth).permitAll()
                .anyRequest().authenticated())

                .formLogin(login -> login
                        .permitAll()
                        .loginPage("/login")
                        .loginProcessingUrl("/login")
                        .defaultSuccessUrl("/", true))

                .logout(LogoutConfigurer::permitAll)

                .csrf(AbstractHttpConfigurer::disable);


        return http.build();
    }


    @Bean
    public static PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }



}
