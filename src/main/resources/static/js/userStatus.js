var contextPath = document.cookie.split("contextPath=")[1].split(";")[0];

setInterval(function() {

    fetch(contextPath + '/status', {
        method: 'GET',
    })
        .then(response => response.text())
        .catch(error => {
            console.error('Error:', error);
        });

}, 5000);